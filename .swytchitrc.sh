OP_ITEM="AWS sandbox (GitLab)"
export AWS_ACCESS_KEY_ID=$(op item get "$OP_ITEM" --vault Private --fields label="fpotter-sandbox-gitlab-aws-access-key-id")
export AWS_SECRET_ACCESS_KEY=$(op item get "$OP_ITEM" --vault Private --fields label="fpotter-sandbox-gitlab-aws-secret-access-key")
export SMTP_USERNAME=$(op read "op://Private/h2ozb5bg4g7dccou32qx4c5xmy/smtp-username")
export SMTP_PASSWORD=$(op read "op://Private/h2ozb5bg4g7dccou32qx4c5xmy/smtp-password")
export AWS_DEFAULT_REGION=us-west-2
export ANSIBLE_TOKEN_REGISTRY_TEST=$(op read "op://Private/ansible.ilovedevops.com/root-write-registry")
