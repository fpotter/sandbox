Congregate
==========

Notes on running Congregate for SM-SaaS migration using Direct Transfer.

## Sandbox

Assumes [GitLab Omnibus](omnibus-aws.md) first.

Congregate host

- FPotterSandboxCongregate
- Ubuntu 22.04
- t2.large
- FPotterSandboxUSWest2KeyPair
- FPotterWebSecurityGroup
- 32 GiB

Remember the "Sandboxing process" from the README here.

Quick-reference commands

```bash
aws ec2 start-instances --instance-id  $(cat .instance-congregate)

aws ec2 stop-instances --instance-id  $(cat .instance-congregate)

aws ec2 describe-instances --instance-ids $(cat .instance-congregate) | jq -r '.Reservations[0].Instances[0].State.Name'

aws ec2 describe-instances --instance-id $(cat .instance-congregate) | jq -r '.Reservations[].Instances[].PublicDnsName' > .domain-congregate

aws ec2 terminate-instances --instance-id  $(cat .instance-congregate)

ssh -i ~/.aws/FPotterSandboxUSWest2KeyPair.pem ubuntu@$(cat .domain-congregate)
```

## Docker engine

```bash
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
sudo usermod -aG docker $USER
reboot now
```

To confirm:

```bash
docker run hello-world
```

With personal access token:

```bash
docker login registry.gitlab.com
```

## Congregate

- Admin rights to instance
- Token with `api`, `read_repository`, `admin_mode`

## Questions

- Is there documentation for the commands besides the `--help` option?
- `validate-config` seems to rely on exceptions to report issues - does that mean it stops when it finds one?
- Both `validate-config` and `list` give messages about "Destination token is currently assigned to a standard user. Some API endpoints may not behave correctly" but I plan to use Direct Transfer so assume I can ignore.

