GPT
===

On the Mac laptop:

```bash
git clone https://gitlab.com/gitlab-org/quality/performance
cd performance
mkdir results
```

Then edited `k6/config/environments/1k.json` with:

```json
    "url": "https://<OMNIBUS_DOMAIN>",
    "user": "<GITLAB_USER>",
```

Set up a PAT with API scope for the Admin user and put it into 1Password. Also start Docker Desktop.

Then:

```bash
export TOKEN=$(op item get "Omnibus GitLab" --fields label=personal-token-api-scope)
docker run -it -e ACCESS_TOKEN=$TOKEN -v $(pwd)/k6/config:/config -v $(pwd)/results:/results gitlab/gpt-data-generator --environment 1k.json
```

...this took a while. It's generating data. Cool.

Woot!

```
GPT data generation finished after 1 hour 27 minutes 52 seconds.
```


_Note: It seems like we didn't really need the full clone of GPT; just the config and tests subdirectories. Everything else came via Docker._

Skip the options file to see what happens (presumably no benchmarks).

```bash
cd ~/git/gitlab-org/quality/performance
export TOKEN=$(op item get "Omnibus GitLab" --fields label=personal-token-api-scope)
docker run -it -e ACCESS_TOKEN=$TOKEN -v $(pwd)/k6/config:/config -v  $(pwd)/k6/tests:/tests -v $(pwd)/results:/results gitlab/gitlab-performance-tool --environment 1k.json
```





