# GET 3K Sandbox

_Notes from a botched attempt. Everything now terminated/destroyed._


```bash

# us-east-2

git clone https://gitlab.com/gitlab-org/gitlab-environment-toolkit.git

# gitlab-fpotter-get-3k-env

sudo apt-get update
sudo apt-get install awscli
aws configure

# Entered credentials and set default region to us-east-2

ssh-keygen -t ed25519 -C "fpotter-get-3k-ssh"
cp .ssh/id_ed25519* gitlab-environment-toolkit/keys/
aws s3api create-bucket --bucket gitlab-fpotter-get-3k-env-deployment --region us-east-2 --create-bucket-configuration LocationConstraint=us-east-2

# http://gitlab-fpotter-get-3k-env-deployment.s3.amazonaws.com/

aws ec2 allocate-address


git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.12.0
echo '. "$HOME/.asdf/asdf.sh"' >>.bashrc
echo '. "$HOME/.asdf/completions/asdf.bash"' >>.bashrc
source .bashrc
asdf plugin add terraform
sudo apt-get install unzip
asdf install terraform 1.2.0
asdf global terraform 1.2.0
mkdir gitlab-environment-toolkit/terraform/environments/gitlab-fpotter-get-3k-env
sudo apt-get install emacs
git clone https://gitlab.com/francispotter/emacs.git ~/.emacs.d
emacs gitlab-environment-toolkit/terraform/environments/gitlab-fpotter-get-3k-env/variables.tf
emacs gitlab-environment-toolkit/terraform/environments/gitlab-fpotter-get-3k-env/main.tf
emacs gitlab-environment-toolkit/terraform/environments/gitlab-fpotter-get-3k-env/environment.tf
export HISTSIZE=0
cat ~/.aws/credentials
export AWS_ACCESS_KEY_ID=""
export AWS_SECRET_ACCESS_KEY=""
cd gitlab-environment-toolkit/terraform/environments/gitlab-fpotter-get-3k-env
terraform init
terraform apply
```

The first time, an error message (see below), so I commented the `elastic_` lines out of `environment.tf` and Terraform ran to completion.

Let's move on to Ansible.

```bash
sudo apt-get install python3-pip
pip3 install ansible
cd ~/gitlab-environment-toolkit/
pip3 install -r ansible/requirements/requirements.txt
echo 'export PATH="/home/ubuntu/.local/bin:$PATH"' >> ~/.bashrc
source ~/.bashrc
ansible-galaxy install -r ansible/requirements/ansible-galaxy-requirements.yml --force
mkdir -p ansible/environments/gitlab-fpotter-get-3k-env/inventory/
emacs ansible/environments/gitlab-fpotter-get-3k-env/inventory/gitlab-fpotter-get-3k-env.aws_ec2.yml
emacs ansible/environments/gitlab-fpotter-get-3k-env/inventory/vars.yml
cd ansible
ansible-playbook -i environments/gitlab-fpotter-get-3k-env/inventory playbooks/all.yml
```

It got a certain distance and then froze; seems like my terminal lost the connection.

(Made a little file called `.source-me` to reload the AWS credentials)

```
ANSIBLE_DEBUG=1 ansible-playbook -i environments/gitlab-fpotter-get-3k-env/inventory playbooks/all.yml
```

Had to reboot. Let's use screen this time.

```bash
ssh -i ~/.aws/fp-aws-sandbox-keypair.pem ubuntu@<IP_ADDRESS>
screen
cd gitlab-environment-toolkit/ansible/
ANSIBLE_DEBUG=1 ansible-playbook -i environments/gitlab-fpotter-get-3k-env/inventory playbooks/all.yml > ~/ansible.log
# ctrl-a, d
```

... got back to the shell prompt but everything froze... and reconnection attepts freeze too :-(.

Theory: project machine is too small. Back in via AWS console:

```
ubuntu@ip-172-31-1-85:~$ free
               total        used        free      shared  buff/cache   available
Mem:          980092      850264       68652        1560       61176       19248
Swap:              0           0           0
```

Reconnecting via `screen -r`, I see the command was "Killed". I'm guessing this requires more RAM on the command host. I also see LOTS of ansible processes in ps.

Also noticing that the `environments` directories are in `.gitignore` so I can't really save my work as-is. Next time let's work from a mirror. Terminating everything now.

Dang. Should have run `terraform destroy`. Now gotta remove the S3 buckets 1-by-1.
