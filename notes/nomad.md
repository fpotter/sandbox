Nomad
=======

Quick-reference commands

```bash
aws ec2 start-instances --instance-id  $(cat local/.instance-nomad)
aws ec2 stop-instances --instance-id  $(cat local/.instance-nomad)
aws ec2 describe-instances --instance-ids $(cat local/.instance-nomad) | jq -r '.Reservations[0].Instances[0].State.Name'
ssh -i ~/.aws/FPotterSandboxUSWest2KeyPair.pem ubuntu@$(cat local/.ip-nomad)
```

AWS instance

- `FPotterSandboxNomad`
- Region `us-west-2`
- Instance type: `c5.xlarge`
- Storage `16 GiB`
- Security group `FPotterWebSecurityGroup` allows 22, 80, 443

```bash
wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install nomad
```

[Next steps](https://developer.hashicorp.com/nomad/tutorials/get-started/gs-start-a-cluster)

