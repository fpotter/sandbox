OneLogin
========

Copy values from OneLogin to GitLab



## Runbook

1. [ ] Sign in to OneLogin admin portal as an admin
    1. [ ] Applications > Add app > GitLab SaaS --> (add it)
    1. [ ] Applications > GitLab SaaS > Configuration > API Connection > API Status --> Enable
    1. [ ] Applications > GitLab SaaS > Provisioning  --> Enable Provisioning + Save
1. [ ] Sign in to GitLab.com as group Owner
    - [ ] GitLab group > Settings > SAML SSO > Enable SAML authentication for this group --> (check on)
1. [ ] Copy settings from GitLab group settings to OneLogin admin portal (below)
1. [ ] Copy settings from OneLogin admin portal to GitLab settings (below)
1. [ ] Test single signon access
1. [ ] Test provisioning

Settings to copy from GitLab to OneLogin

| **from** GitLab group settings | **to** OneLogin admin portal - Application GitLab SaaS |
| --- | --- |
| General > Group name | Configuration > Application Details > Group Name |
| SAML SSO > GitLab metadata URL (_only the token part_) | Configuration > Application Details > Token |
| SAML SSO > SCIM token > Generate SCIM token > SCIM API endpoint URL | Configuration > API Connection > SCIM Base URL |
| SAML SSO > SCIM token > Your SCIM token | Configuration > API Connection > SCIM bearer token |

Settings to copy from OneLogin to GitLab

| Value in OneLogin | GitLab SAML Single Sign On Setting | 
| --- | --- |
| Applications > GitLab (SaaS) > SSO > SAML 2.0 Endpoint (HTTP) | Identity provider single sign-on URL | 
| Applications > GitLab (SaaS) > SSO > X.509 Certificate > View Details > Fingerprint | Certificate fingerprint | 

Note: users will receive an email from GitLab and have to click the link (until domain verification is set up)

## Resources

- [Enterprise users](https://docs.gitlab.com/ee/user/enterprise_user/)
- [OneLogin Introduction to User Provisioning](https://onelogin.service-now.com/support?id=kb_ahttps://fpotter-gitlab.onelogin.com/rticle&sys_id=96aba0af870e8a90f7b8a7dd3fbb351b&kb_category=7506a530db185340d5505eea4b9619ba)
- [Configure SCIM for GitLab.com groups](https://docs.gitlab.com/ee/user/group/saml_sso/scim_setup.html#configure-an-identity-provider)
- [SAML SSO for GitLab.com groups](https://docs.gitlab.com/ee/user/group/saml_sso/)
- [OneLogin Configuring SSO for SAML-Enabled Applications](https://onelogin.service-now.com/support?id=kb_article&sys_id=ae7b78451b6606509a47ec29b04bcb15&kb_category=50984e84db738300d5505eea4b961913)

## Journal

20240531

- https://fpotter-gitlab.onelogin.com trial has expired. Creating a new account.
- fpotter-gitlab-02 fpotter+onelogin-02@gitlab.com
- OneLogin doc: "If you intend to configure both SAML and provisioning for an application, you must enable provisioning first and only then complete your SAML configuration" but GitLab doc: "Group single sign-on must be configured" before SCIM.
- OneLogin doc: user provisioning "requires a OneLogin subscription that includes Advanced Directory." It's not clear whether the trial account qualifies.
- OneLogin doc: "some apps in OneLogin provide SAML-based "just-in-time" (JIT) provisioning."
- When I enabled the GitLab SaaS app in the OneLogin admin portal, I did not get "API username" or "API password" fields and didn't sign in anywhere (?)
- Note I still have a user from the previous attempt (https://gitlab.com/fpotter-onelogin02)
- Tried turning provisioning on then adding a user. Username: u-02-01. User exists in both systems, with the correct username, and is a member of the group, but 404s when trying to view the group
    - Try disconnect SSO in user preferences - can browse
    - Go to SSO URL - authorize - back to can't access
    - Oops. In Configuration I had the "group name" as the whole URL

