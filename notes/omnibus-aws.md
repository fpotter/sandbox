Omnibus notes
=============

Quick-reference commands

```bash
aws ec2 start-instances --instance-id  $(cat local/.instance-omnibus)
aws ec2 stop-instances --instance-id  $(cat local/.instance-omnibus)
aws ec2 describe-instances --instance-ids $(cat local/.instance-omnibus) | jq -r '.Reservations[0].Instances[0].State.Name'
ssh -i ~/.aws/FPotterSandboxUSWest2KeyPair.pem ubuntu@code1.ilovedevops.com
```

---

- Region `us-west-2`
- Instance type: `c5.xlarge`
- Storage `16 GiB`
- Security group `FPotterWebSecurityGroup` allows 22, 80, 443

Install
-------

[Follow the steps](https://about.gitlab.com/install/#ubuntu)

```bash
sudo apt update
sudo apt-get install -y curl openssh-server ca-certificates tzdata perl
```

(Skip Postfix installation so we can disable email or use the AWS service)

```bash
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
sudo EXTERNAL_URL="https://<domain-name>" apt-get install gitlab-ee
sudo cat /etc/gitlab/initial_root_password
```

Then:

- Browse http://\<DOMAIN\> and confirm redirect to https.
- Sign in with `root` and that password I copied.
- Click the button to deactivate signups
- ... and we're in!

Configure SES
-------------

To enable email and add myself as a real user:

- Add an "identity" in AWS SES. Use your domain as the sending domain.
- Set up EasyDKIM with my domain name registrar (takes 72 hrs to verify?)
- Send a test email through AWS console (received by some kind of simulator)
- Request production access for the SES account

Edits to `gitlab.rb`:

```ruby
gitlab_rails['smtp_enable'] = true
gitlab_rails['smtp_address'] = "email-smtp.us-west-2.amazonaws.com"
gitlab_rails['smtp_port'] = 587
gitlab_rails['smtp_user_name'] = "<SMTP_USERNAME>"
gitlab_rails['smtp_password'] = ""
gitlab_rails['smtp_domain'] = "<DOMAIN>"
gitlab_rails['smtp_authentication'] = "login"
gitlab_rails['smtp_enable_starttls_auto'] = true
```

_Note: Would I put the SMTP password in `secrets.json`?_

To edit:

```bash
sudo -E emacs /etc/gitlab/gitlab.rb
```

Then:

```bash
sudo gitlab-ctl reconfigure
```

Invite myself as a new user via email with Admin privileges.

Delete the initial placeholder admin user.

Explore Admin Area
------------------

- Sidekiq jobs
- Background migrations
- ... and more

Explore maintenance commands
----------------------------

```bash
sudo gitlab-ctl status
sudo tail -f /var/log/gitlab/nginx/gitlab_access.log
sudo gitlab-ctl tail gitlab-rails
sudo gitlab-ctl stop
sudo gitlab-ctl start
sudo gitlab-psql  # Then \dt just for fun
sudo ls -al /var/opt/gitlab/gitlab-rails
```

Explore Rails console
---------------------

```bash
sudo gitlab-rails console
```

Explore export/import
---------------------

Perform project export from GitLab.com in the UI, and import to Omnibus.

Had to enable GitLab Export as an Import Source in Admin.

Upgrade
-------

Wait for background migrations to complete in the Admin area!

```bash
sudo apt-get update
sudo apt-cache madison gitlab-ee | grep 16.3
sudo apt install gitlab-ee=16.3.0-ee.0
sudo gitlab-rake gitlab:env:info
sudo ls -al /var/opt/gitlab/backups
```

Back up
-------

Wait for background migrations to complete in the Admin area!

```bash
sudo ls -al /var/opt/gitlab/backups
sudo gitlab-backup create
sudo ls -al /var/opt/gitlab/backups
sudo stat /var/opt/gitlab/backups<NEWEST_FILE>
```


AWS S3
------

So we have a place to put backups.

```bash
sudo apt-get install awscli
```

... and we left off here