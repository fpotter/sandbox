Runner with Podman
=============

Quick-reference commands

```bash
aws ec2 start-instances --instance-id  $(cat local/.instance-runner)
aws ec2 stop-instances --instance-id  $(cat local/.instance-runner)
aws ec2 describe-instances --instance-ids $(cat local/.instance-runner) | jq -r '.Reservations[0].Instances[0].State.Name'
ssh -i ~/.aws/FPotterSandboxUSWest2KeyPair.pem ubuntu@$(cat local/.ip-runner)
```

---

- Region `us-west-2`
- Instance type: `c5.large`
- Storage `8 GiB`
- Security group `FPotterSSHOnlySecurityGroup` allows 22
- OS `Ubunto 24.04`

Install
-------

```bash
sudo passwd ubuntu  # Remember password
sudo apt update
sudo apt-get install -y curl podman slirp4netns systemd-container
podman --version  # Must be >=4.2
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt install gitlab-runner
sudo usermod --add-subuids 100000-165535 --add-subgids 100000-165535 gitlab-runner
sudo passwd gitlab-runner  # remember password - necessary?
machinectl shell gitlab-runner@.host  # Enter ubuntu password
systemctl --user --now enable podman.socket
systemctl status --user podman.socket
exit
sudo loginctl enable-linger gitlab-runner
```

Noted this from the `systemctl status` command:

```
/run/user/998/podman/podman.sock
```

``
/run/user/999/podman/podman.sock (Stream)
```

Edited `config.toml`

```toml
[[runners]]
  name = "podman-test-runner-2022-06-07"
  url = "https://gitlab.com"
  token = "________"
  executor = "docker"
  [runners.docker]
    host = "unix:////run/user/____/podman/podman.sock"
    tls_verify = false
    image = "alpine"
    privileged = true
```

TODO
----

- https://docs.gitlab.com/runner/executors/docker.html#create-a-network-for-each-job

