Docker
======

Running GitLab within Docker

Quick-reference commands

```bash
aws ec2 start-instances --instance-id  $(cat local/.instance-docker)
aws ec2 stop-instances --instance-id  $(cat local/.instance-docker)
aws ec2 describe-instances --instance-ids $(cat local/.instance-docker) | jq -r '.Reservations[0].Instances[0].State.Name'
ssh -i ~/.aws/FPotterSandboxUSWest2KeyPair.pem ubuntu@$(cat local/.ip-docker)
```

----

- Instance type: `c5.xlarge`
- Storage `16 GiB`
- Security group  allows 22, 80, 443


Personal preferences


Then log out and back in, so I've got my prompts and emacs settings.

Install Docker

```bash
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
sudo docker run hello-world
```

(_stopped here_)

Install GitLab on Docker

```bash
sudo mkdir /srv/gitlab
sudo chmod 777 /srv/gitlab
echo "export GITLAB_HOME=/srv/gitlab" > ~/.zdotdir/.zlocal
source ~/.zdotdir/.zlocal
```

Leaving out the SSH port to avoid port conflict:

```bash
sudo docker run --detach \
  --hostname <DOMAIN> \
  --publish 443:443 --publish 80:80 \
  --name gitlab \
  --restart always \
  --volume $GITLAB_HOME/config:/etc/gitlab \
  --volume $GITLAB_HOME/logs:/var/log/gitlab \
  --volume $GITLAB_HOME/data:/var/opt/gitlab \
  --shm-size 256m \
  gitlab/gitlab-ee:latest
```

See if it worked:

```bash
sudo docker exec gitlab gitlab-ctl status
```

And [we have an instance](http://<DOMAIN>/users/sign_in)!

The web interface is running through HTTP, so we wouldn't use this much.

Check my version:

```bash
sudo docker exec gitlab gitlab-rake gitlab:env:info
```

It says `16.3.0-ee`

## General notes on working with Docker

Stop all the containers

```bash
docker stop $(docker ps -a -q)
```

Remove all the containers

```bash
docker rm $(docker ps -a -q)
```