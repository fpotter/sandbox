Kubernetes
==========

I need to create an EKS cluster and deploy a test app to it from GitLab for a pro services POC.

References
----------

- [Create an Amazon EKS cluster](https://docs.gitlab.com/ee/user/infrastructure/clusters/connect/new_eks_cluster.html)
- [Cluster Management Project](https://docs.gitlab.com/ee/user/clusters/management_project_template.html)
- [Use Auto DevOps to deploy an application to Amazon Elastic Kubernetes Service (EKS)](https://docs.gitlab.com/ee/topics/autodevops/cloud_deployments/auto_devops_with_eks.html)
- [Installing the agent for Kubernetes | GitLab](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html)
- [Using GitLab CI/CD with a Kubernetes cluster](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html)
- [Using GitOps with a Kubernetes cluster](https://docs.gitlab.com/ee/user/clusters/agent/gitops.html)
- [Tutorial: Set up Flux for GitOps](https://docs.gitlab.com/ee/user/clusters/agent/gitops/flux_tutorial.html)
- [StackOverflow question](https://stackoverflowteams.com/c/gitlab-customer-success/questions/2783)


Attempt #1: Create a cluster in AWS console and install agent using Helm commands
--------

Cluster: `fpotter-test01-k8s-cluster`

Some commands I ran. Note the dotted files are local-only because they contain secrets.

```bash
aws eks update-kubeconfig --region us-east-2 --name fpotter-test02-k8s-cluster
source .helm-ingress
source .help-agent
kubectl get all -n gitlab-managed-apps --selector app.kubernetes.io/instance=ingress-nginx
```

Attempt #2: Create a cluster in AWS console and use a cluster management process
--------

My project: https://gitlab.com/fpotter/kubernetes

... stopped when I realized the new cluster has no nodes so can't run anything. Presumably the next step is to create a nodegroup, but then I discovered the Terraform project (below)

Attempt #3: Use the Terraform-based example
------

Following [this](https://docs.gitlab.com/ee/user/infrastructure/clusters/connect/new_eks_cluster.html) doc and [this](https://gitlab.com/gitlab-org/configure/examples/gitlab-terraform-eks) project.

My project: https://gitlab.com/fpotter/terraform-eks

- User: `fpotter-terraform-eks-user`
- Policy: `GitLabTerraformEKSPolicy`
- Cluster name: `fpotter-test03-k8s-cluster`

**[Error](https://gitlab.com/gitlab-org/configure/examples/gitlab-terraform-eks/-/issues/20)** seems like a bug in the underlying project

Attempt #4: Follow James Sandlin's approach
-------

From James:

> This [group](https://gitlab.com/sandlin/aws) has it all.
>   1. [Create the network & EKS](https://gitlab.com/sandlin/aws/gitlab-terraform-eks)
>   2. [Connector config](https://gitlab.com/sandlin/aws/eks-agentk-connector)
>   3. [Install GitLab Agent for K8S](https://gitlab.com/sandlin/aws/eks-gitlab-agent)
>   4. [Manage apps via agent](https://gitlab.com/sandlin/aws/eks-cluster-apps)
>
> Currently, with my IAM definition there is a connection error between the k8s manager <-> nodes. In theory, it’ll work fine with the owner account (although inseure)
>
> I have everything autonated & working for GCP in this group, which is pretty much the same thing.

Reviewed James' material, then elected to return to the core Terraform-based example (see https://gitlab.com/fpotter/terraform-eks)

An image for running `terraform` and `kubectl` commands lives at https://gitlab.com/fpotter/ops-tools.


Attempt #5: Viktor's new approach
----------

[WORKS!](https://gitlab.com/fpotter/eks-gitlab-terraform)


Discussions
-----------

Viktor Nagy:

> Installing the agent for Kubernetes + Flux (if you want pull-based; for push-based the agent is enough)
>
> As it’s a POC, I recommend the Tutorial you already found.
> 
> Use Flux to deploy apps OR use CI/CD optionally with the AutoDevOps template
> 
> You can create the EKS cluster on the console or with eksctl. No need to use Terraform.
>
> As you want to focus on AutoDevOps, no need to use Flux.
> 
> I’d still install the agent to show the deployments under the GitLab UI.
>
> Flux is “only” a GitOps tool.
>
> The agent provides a bidirectional channel between Flux and GitLab, allowing GitLab to show a real-time cluster state in the GitLab GUI (the request goes from GitLab to the cluster), or connecting to the cluster from GitLab pipelines (the request goes from a Runner to the cluster), or surfacing a container security scan to GitLab from the cluster (the request goes from the cluster to GitLab). The agent is the core tool to integrate GitLab with a K8s cluster. For Flux, the agent adds some features to simplify setup and usage of Flux, but Flux is not needed for the majority of the agent features.
>
> - agent
>     - push-based -> “CICD workflow”
>     -  GUI for Kubernetes
>     -  operational security scanning
>     -  Flux reconciler setup (if Flux is installed)
> - flux -> pull-based -> “GitOps workflow”
> - certificate-based -> no longer possible
>
> You can use the agent for a Kubernetes GUI together with Flux too.

James Sandlin:

> I always run things locally before trying to get the pipeline to pass.
>
> I use the remote state management so I can ensure TF state is identical if I run locally or if I run via Gitlab Pipeline. If you look in any of my TF projects, I have a install.sh which loads the TF creds & gitlab token from local files then sets them as env vars then calls TF. This allows me to execute TF locally just like it’ll be executed when in a pipeline. NOTE: You’ll need to modify the URL of each repo to point to the correct GitLab project in order to store state.

Darwin Sanoy:

> I've been asking for a.long time that we stop publiahing a very incomplete EKS terraform and instead use AWS EKS Blueprints (https://github.com/aws-ia/terraform-aws-eks-blueprints) as I did for this article: https://about.gitlab.com/blog/2023/05/24/eks-fargate-runner/ (but don't run kube-system as fargate or it will cost you a fortune compared to small instances.