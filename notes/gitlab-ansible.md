# GET 1K Sandbox (Ansible only with static inventory)

## Quick-reference commands

```bash
aws ec2 start-instances --instance-id  $(cat local/.instance-ansible)
aws ec2 stop-instances --instance-id  $(cat local/.instance-ansible)
aws ec2 describe-instances --instance-ids $(cat local/.instance-ansible) | jq -r '.Reservations[0].Instances[0].State.Name'
ssh -i ~/.aws/FPotterSandboxUSWest2KeyPair.pem ubuntu@ansible.ilovedevops.com
```

## Provisioning (AWS console)

Launch instance

- Name `FPotterSandboxGitLabAnsible`
- Region `us-west-2`
- Ubuntu 22.04 LTS
- Instance type: `c5.xlarge`
- Storage `16 GiB`
- Security group `FPotterWebSecurityGroup` allows 22, 80, 443

Create Elastic IP (`44.236.96.120`)

Direct domains to the IP address

- `ansible.ilovedevops.com`
- `registry.ansible.ilovedevops.com`
- `docs.ansible.ilovedevops.com`

## Running Ansible

```bash
docker run -it --rm\
  -e SMTP_USERNAME="$SMTP_USERNAME" -e SMTP_PASSWORD="$SMTP_PASSWORD" \
  -v ~/.aws:/gitlab-environment-toolkit/keys \
  -v ./conf/ansible/environments/sandbox:/gitlab-environment-toolkit/ansible/environments/sandbox \
  -v ./local:/gitlab-environment-toolkit/ansible/local \
  registry.gitlab.com/gitlab-org/gitlab-environment-toolkit:3.4.0
```

Inside the container:

```bash
cd ansible
```

Run the playbook:

```bash
ansible-playbook -i environments/sandbox/inventory/ playbooks/all.yml
```

Sign in and change the root password!

Note there's no object storage - files such as uploads are stored in `/var/opt/gitlab/gitlab-rails` and `/var/opt/gitlab/gitlab-rails/shared` (and maybe other places)


## Git-LFS

Tested: Git-LFS works with GET-based Ansible Omnibus GitLab out-of-the-box. No customization necessary.

Update 2024-10-17: Can't reproduce the test. How to tell whether files are stored via LFS? Expecting to see a directory at `/opt/gitlab/embedded/service/gitlab-rails/public/shared`.



## GitLab Pages

- Daemon can run on same machine as single-node Omnibus.
- It is included with Omnibus GitLab
- Have to use custom config There are [settings](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/ansible/roles/common_vars/defaults/main.yml#L200) for Pages storage storage in GET but it's not in the `gitlab.rb` [template](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/ansible/roles/gitlab_rails/templates/gitlab-rails.gitlab.rb.j2) and there is an [open issue](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/issues/773).
- We're going to use a [basic domain](https://docs.gitlab.com/ee/administration/pages/#for-namespace-in-url-path-without-wildcard-dns) and not worry about the [Public Suffix List](https://docs.gitlab.com/ee/administration/pages/#add-the-domain-to-the-public-suffix-list)
- Assume certificate included in the SAN at `/etc/gitlab/ssl/domain.name.crt` and `.key`
- Omnibus does NOT support LetsEncrypt integration for GitLab Pages

We cheated and installed certbot on the machine, then copied the cert off the machine.

Created an A-Record in DNS, then:

```bash
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
sudo gitlab-ctl stop
sudo certbot certonly --standalone
sudo cp /etc/letsencrypt/live/docs.ansible.ilovedevops.com/fullchain.pem /etc/gitlab/ssl/docs.ansible.ilovedevops.com.crt
sudo cp /etc/letsencrypt/live/docs.ansible.ilovedevops.com/privkey.pem /etc/gitlab/ssl/docs.ansible.ilovedevops.com.key
```

## Certificate upload

- External SSL terminated with GitLab Rails NGINX
- User-provided certificates (even though we generate it using certbot for the example)

Requirements:

- Certificate and key files are named in the formats `<hostname>.pem` and `<hostname>.key` respectively
- Certificates must contain Subject Alternative Name(s) (SAN) as Common Name (CN) use only is deprecated. SAN entries should cover any additional hostnames that are expected to be used with the environment. This includes additional features such as the Container Registry (registry.<external_host>).
- Certificate and key files are located in the ansible/environments/<env_name>/files/certificates folder
- Certificate file contains the full chain.

Issued a SAN cert for all 3 names.

### SMTP

Now requires settings in `vars.yml` and environment variables for `SMTP_USERNAME` and `SMTP_PASSWORD`.

Required DKIM for SES.

### Confirm container registry works

```bash
docker build -t registry.ansible.ilovedevops.com/root/test/hello hello-world/
echo "$ANSIBLE_TOKEN_REGISTRY_TEST" | docker login registry.ansible.ilovedevops.com -u root --password-stdin
docker push registry.ansible.ilovedevops.com/root/test/hello
```
