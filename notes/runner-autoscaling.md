
Runner Autoscaling
==================

Resources:

- [Vending machine](https://gitlab.com/guided-explorations/aws/gitlab-runner-autoscaling-aws-asg)
- [Create a group runner with a runner authentication token](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#create-a-group-runner-with-a-runner-authentication-token)

Actions

1. Launch the Omnibus instance
1. Browse the Omnibus instance
1. Create a group with a name like `ci-group`
1. Create a project with a name like `ci-project` and CI config using the Bash template, so there's something to run
1. That the pipeline is stuck in "Pending" state because of no runners
1. To generate a Runner Authentication Token:
    1. In the group, go to build > runners
    1. Click "add new runner"
    1. Fill out the form. For sandboxing, check "run untagged jobs"
    1. Submit the form
    1. Copy the token
1. Click the "ARM64 Amazon Linux 2 Docker HA with Manual Scaling and Optional Scheduling. Non-spot." easy button.
1. Sign in to the AWS console
1. Complete the form:
   - GitLab Instance URL: _Omnibus URL_
   - Token: _token_
   - Number of instances: 1
   - Go with all the defaults for now
   - Check the checkboxes about IAM resources
   - Click "Create Stack"
1. Wait until CloudFormation completes
1. In the GitLab UI
   1. go back to the Group Runners list and confirm the runner is "online"
   1. go back to the project and confim the pipeline completed


Questions

- Default instance types are `m6g.large` for primary, plus "d" for "second priority" (set in vending machine) - why?
