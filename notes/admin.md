# Static Admin

_Deserves a better name_

_Also probably deserves its own project_

Using an SSH-only box for static IP. Leave this one running.

Only works from home IP address.

```bash
ssh-static
```

I've got a script :-)

## Proxy service

### Installation

```bash
sudo apt-get install nginx
cp /etc/nginx/nginx.conf conf/
```

- Confirmed Nginx is running with local curl
- Opened line to home network in security group
- Added entry for `gitlab.com.proxy` to `/etc/hosts` at home (for starters)
- (Uncomment the `server_names_hash_bucket_size` line in `nginx.conf`)

### Self-signed certificate
```bash
sudo mkdir -p /etc/nginx/ssl
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl/gitlab.com.proxy.key -out /etc/nginx/ssl/gitlab.com.proxy.crt
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl/proxy.fpotter.com.key -out /etc/nginx/ssl/proxy.fpotter.com.crt
```

### LetsEncrypt certificate

Set up a DNS A-record

```
Name:	proxy.fpotter.com
Address: 35.182.205.202
```

Installed certbot

```bash
sudo apt-get install certbot
sudo apt-get install python3-certbot-nginx
```

This is a little odd because the file needs to exist in `sites-available` already.

Must open 80 to the internet before performing:

```bash
sudo certbot --domains proxy.fpotter.com
```

(Close port 80 immediately)

That writes to the config file in `sites-available` so be sure to catch the values in your local copy if you've made it already.

### Configuration

One time

```bash
sudo ln -s /etc/nginx/sites-available/com-fpotter-proxy.conf /etc/nginx/sites-enabled/
```

My files ended up looking like...

`nginx.conf`
```
user ubuntu;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
	worker_connections 768;
}

http {
	sendfile on;
	tcp_nopush on;
	types_hash_max_size 2048;
	server_names_hash_bucket_size 64;
	include /etc/nginx/mime.types;
	default_type application/octet-stream;
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3;
	ssl_prefer_server_ciphers on;
        log_format custom_log '$remote_addr - $remote_user [$time_local] "$request" '
                          '[$request_filename] $status $body_bytes_sent "$http_referer" '
                          '"$http_user_agent"';
        access_log /var/log/nginx/access.log custom_log;
	error_log /var/log/nginx/error.log;
	gzip on;
	include /etc/nginx/conf.d/*.conf;
	include /etc/nginx/sites-enabled/*;
}
```

`com-fpotter-proxy.conf`
```
server {
    server_name proxy.fpotter.com;
    listen 443 ssl;

    ssl_certificate /etc/letsencrypt/live/proxy.fpotter.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/proxy.fpotter.com/privkey.pem;

    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers HIGH:!aNULL:!MD5;
    ssl_prefer_server_ciphers on;

    location / {
	proxy_pass https://gitlab.com;
	proxy_ssl_server_name on;
    }
}
```

To adjust:

```bash
emacs conf/nginx.conf
sudo cp conf/nginx.conf /etc/nginx/
emacs conf/com-fpotter-proxy.conf
sudo cp conf/com-fpotter-proxy.conf /etc/nginx/sites-available/
sudo nginx -t
sudo systemctl restart nginx
```

### Troubleshooting

```bash
sudo tail -f /var/log/nginx/access.log
sudo tail -f /var/log/nginx/error.log
```

The proxy only works for the API. The web UI requires a captcha, which seems to use referer and timing tricks to detect the proxy.
