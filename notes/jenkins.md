Jenkins
=======

Quick-reference commands

```bash
aws ec2 start-instances --instance-id  $(cat .instance-jenkins)
aws ec2 stop-instances --instance-id  $(cat .instance-jenkins)
aws ec2 describe-instances --instance-ids $(cat .instance-jenkins) | jq -r '.Reservations[0].Instances[0].State.Name'
ssh -i ~/.aws/fp-aws-sandbox-keypair.pem ubuntu@$(cat .domain-jenkins)
```

Instance type: `c4.large`
Storage: `16GB`
OS: `ubuntu 22.04`

---

Tried the Community AMI called "SupportedImages Jenkins on Ubuntu 22.04 20230525-prod-zgjkkuehyo4dy" - no sign of Jenkins after instantiating it. Later realized maybe I had to open port 8080 in the security group. Too late now. I'm installing it myself.

Jenkins installation:

```bash
sudo apt install openjdk-17-jre
curl -fsSL https://pkg.jenkins.io/debian/jenkins.io-2023.key | sudo tee \
  /usr/share/keyrings/jenkins-keyring.asc > /dev/null
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
  https://pkg.jenkins.io/debian binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null
sudo apt-get update
sudo apt-get install jenkins
```

Troubleshooting notes:

```bash
systemctl status jenkins.service
journalctl -xeu jenkins.service
journalctl -u jenkins.service
java -version
systemctl status jenkins.service
```

It works! (browse port 8080)

1. Follow instructions in browser to copy initial admin password from `/var/lib/jenkins/secrets/initialAdminPassword`
1. Click "Install suggested plugins" - It installs some conventional-sounding plugins like "Folders" and "Git" but also specific ones like "Ant" and "Gradle"
1. Completed the form for an admin user with an email address I control
1. Accepted `http://<DOMAIN>:8080/` as the URL (later we ought to add an HTTPS layer)
1. Click "Start Using Jenkins" and get the home page
1. Click "Create a job"
1. Enter a name and click "Freestyle Job" and continue
1. Fill out the "Configure" form for Busy, as well as I can - it asks for a "build shell" (BASH script?) so I'm just putting an echo - no sign of Docker at this point
1. Save all that
1. Click "Build now"

Found the "console output" in the UI:

```
Started by user Francis Potter
Running as SYSTEM
Building in workspace /var/lib/jenkins/workspace/Busy Sandbox
The recommended git tool is: NONE
No credentials specified
Cloning the remote Git repository
Cloning repository https://gitlab.com/steampunk-wizard/projects/busy.git
 > git init /var/lib/jenkins/workspace/Busy Sandbox # timeout=10
Fetching upstream changes from https://gitlab.com/steampunk-wizard/projects/busy.git
 > git --version # timeout=10
 > git --version # 'git version 2.34.1'
 > git fetch --tags --force --progress -- https://gitlab.com/steampunk-wizard/projects/busy.git +refs/heads/*:refs/remotes/origin/* # timeout=10
 > git config remote.origin.url https://gitlab.com/steampunk-wizard/projects/busy.git # timeout=10
 > git config --add remote.origin.fetch +refs/heads/*:refs/remotes/origin/* # timeout=10
Avoid second fetch
 > git rev-parse refs/remotes/origin/main^{commit} # timeout=10
Checking out Revision 3bf3ff062bcb4dc412f54896b2867f982b2a0a9e (refs/remotes/origin/main)
 > git config core.sparsecheckout # timeout=10
 > git checkout -f 3bf3ff062bcb4dc412f54896b2867f982b2a0a9e # timeout=10
Commit message: "Bug: Characters not inserting in editor"
First time build. Skipping changelog.
[Busy Sandbox] $ /bin/sh -xe /tmp/jenkins15212111234769114632.sh
+ echo Hello from Jenkins!
Hello from Jenkins!
Finished: SUCCESS
```

Confirmed that is the latest ref in main for that project, so it accessed GitLab successfully.

Exploring behind the scenes

| Location | Contents |
| --- | --- |
| `/var/lib/jenkins/jobs/Busy\ Sandbox/config.xml` | Freestyle job configuraton, including the shell script |
| `/var/lib/jenkins/workspace/Busy\ Sandbox` | The cloned repo |
| `/var/lib/jenkins/jobs/Busy\ Sandbox/builds/1/log` | Build history/log |
| `/var/lib/jenkins/jobs/Busy\ Sandbox/builds/1/build.xml` | Data on this specific build, including plugin-specific data such as the Git ref |

```bash
ls -1F /var/lib/jenkins
```

```config.xml
hudson.model.UpdateCenter.xml
hudson.plugins.git.GitTool.xml
identity.key.enc
jenkins.install.InstallUtil.lastExecVersion
jenkins.install.UpgradeWizard.state
jenkins.model.JenkinsLocationConfiguration.xml
jenkins.telemetry.Correlator.xml
jobs/
logs/
nodeMonitors.xml
nodes/
plugins/
queue.xml
queue.xml.bak
secret.key
secret.key.not-so-secret
secrets/
updates/
userContent/
users/
workspace/
```

This works: `ls -al /var/lib/jenkins/jobs/Busy\ Sandbox/builds/1` so maybe "build" is a child of "job"

The big catch at this stage is there's no Python or Docker on the Jenkins machine where builds run.

Pipeline
-------------

Ideally we'd run Docker on an agent. But let's keep it simple, and see if we can run the Docker plugin alongside Jenkins itself.

According to the [docs](https://www.jenkins.io/doc/book/pipeline/docker/), we can run builds within Docker without a plugin if we use a "Pipeline".

Note: Options are "Pipeline script" (in the UI) or "Pipeline script from SCM".

Made a simple pipeline:

```groovy
pipeline {
    agent any 
    stages {
        stage('Stage 1') {
            steps {
                echo 'Hello from a Pipeline!' 
            }
        }
    }
}
```

Other plays:

- Pull the Jenkinsfile from the Git repo. [Example](https://gitlab.com/steampunk-wizard/sandbox/jenkins-sandbox).
- Configure the GitLab integration and run a freestyle project
- Run a pipeline project through the integration, using the Jenkinsfile in the project

Observation: Have to set the Project Name in the Jenkins integration in GitLab AND the repo URL in Jenkins. Doing so requires administrative overhead and risks configuration errors and long troubleshooting timelines.

Look for Jenkins output in an MR.

Set the "Branch Specifier (blank for 'any')" to blank in the job configuration. The job runs on a commit to the MR, but the console output says:

```
Started by GitLab push by Francis Potter (GitLab)
java.lang.IllegalArgumentException: Invalid refspec refs/heads/**
	at org.eclipse.jgit.transport.RefSpec.checkValid(RefSpec.java:599)
	at org.eclipse.jgit.transport.RefSpec.<init>(RefSpec.java:203)
	at org.eclipse.jgit.transport.RefSpec.<init>(RefSpec.java:255)
	at jenkins.plugins.git.GitSCMFileSystem$BuilderImpl.build(GitSCMFileSystem.java:404)
	at jenkins.scm.api.SCMFileSystem.of(SCMFileSystem.java:219)
	at org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition.create(CpsScmFlowDefinition.java:118)
	at org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition.create(CpsScmFlowDefinition.java:70)
	at org.jenkinsci.plugins.workflow.job.WorkflowRun.run(WorkflowRun.java:311)
	at hudson.model.ResourceController.execute(ResourceController.java:101)
	at hudson.model.Executor.run(Executor.java:442)
Finished: FAILURE
```

It appears that Jenkins sets the refspec to (invalid?) `**` when we make it blank. Is that a regexp? Maybe we need to try `.*` or something.
