FPotter Professional Ops
=====================

[Everything is in draft](https://handbook.gitlab.com/handbook/values/#everything-is-in-draft) and much of the contents are notes from sandbox projects.

Also see the [Auto DevOps POC](https://gitlab.com/gl-demo-ultimate-fpotter/autodevops-poc/)

## Sandboxing process

Sandboxes run on AWS. I use Elastic IP addresses to connect domain names (from my personal domain registrar account) but rigorously stop the instances at the end of every working session to minimize ongoing costs.

Locally, I run `awscli` to control instances and other configuration.

```bash
brew install awscli
aws configure # And options, including a new IAM role
```

More recently, I've started using Docker and [ops tools](https://gitlab.com/fpotter/ops-tools) to isolate the toolset from the desktop.

EC2 instances run the latest LTS version of Ubuntu. I typically set up the instance with personal preferences thus:

```bash
sudo apt-get update
sudo apt-get install emacs zsh tmux
git clone https://gitlab.com/francispotter/emacs.git ~/.emacs.d
git clone https://gitlab.com/francispotter/zshrc.git ~/.zdotdir
echo "export ZDOTDIR=~/.zdotdir" > ~/.zshenv
sudo usermod --shell /bin/zsh ubuntu
exec zsh
```

Notes are sanitized to remove specific information, which is kept locally in dotfiles.

Generally defaulting to `us-west-2` but there might be exceptions.

When using `tmux`: ctrl-b d to detach.
