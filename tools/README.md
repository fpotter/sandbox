# ops-tools

A Docker image containing useful tools such as `terraform` and `kubectl`.

Usage

```bash
docker run -it -v "$(pwd):/app" \
-e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
registry.gitlab.com/fpotter/ops-tools/ops-tools /bin/bash
```
